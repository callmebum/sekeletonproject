package call.me.bum.sekeletonproject.views;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import call.me.bum.sekeletonproject.R;
import call.me.bum.sekeletonproject.adapters.RecyclerViewAdapter;

public class DemoActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_demo);
    initRecyclerView();
  }

  private void initRecyclerView() {
    RecyclerView recyclerView = findViewById(R.id.recycler_view);
    recyclerView.setAdapter(new RecyclerViewAdapter(this));
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
  }
}
