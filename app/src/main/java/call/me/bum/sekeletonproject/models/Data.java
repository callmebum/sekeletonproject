package call.me.bum.sekeletonproject.models;

public class Data {

  private String mTitle;
  private String mImageUrl;

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public String getImageUrl() {
    return mImageUrl;
  }

  public void setImageUrl(String imageUrl) {
    mImageUrl = imageUrl;
  }
}
